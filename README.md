# Piveau utils corsproxy

This is a service, which will proxy any get request and add required cors headers, if posible

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [API](#api)
1. [Configuration](#configuration)
    1. [Environment](#environment)
    1. [Logging](#logging)
1. [License](#license)



## Build

Requirements:
 * Git
 * Maven 3
 * Java 11

```bash
$ git clone ...
$ mvn package
```
 
## Run

```bash
$ java -jar target/corsproxy.jar
```

## Docker

Build docker image:
```bash
$ docker build -t piveau/corsproxy .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 piveau/corsproxy
```

## API

To proxy a request to e.g. `https://example.com/path/?example=parameter`, the parameter `uri` has to be used. 

Additionally, the `origin` header for this request has to indicate a valid origin. Which origin is valid can be configured via environment variables.

An example request via curl would look like

```shell
curl --request GET \
  --url 'https://proxy-domain.tld/?uri=https%3A%2F%2Fexample.com%2Fpath%2F%3Fexample%3Dparameter' \
  --header 'Origin: https://piveau.eu'

```

## Configuration

### Environment

| Key | Description | Default | Example |
| :--- | :--- | :--- |:--- |
| PROXY_CORS_DOMAINS | all domains that are allowed to call this service | [] |["localhost", "piveau.eu"] |
| PROXY_PORT | the port this service is listening on | 8080 |8080 |

## License

[Apache License, Version 2.0](LICENSE.md)

## Dependencies and their licenses

***
This [THIRD-PARTY.txt](third-party%2FTHIRD-PARTY.txt) contains a list of the dependencies and their licenses. 
