# ChangeLog

## Unreleased

## [0.0.2](https://gitlab.fokus.fraunhofer.de/piveau/metrics/piveau-utils-cors-proxy/tags/0.0.1) (2020-12-03)

**Added:** `X-Auth-Token` to allowed header list


## [0.0.1](https://gitlab.fokus.fraunhofer.de/piveau/metrics/piveau-utils-cors-proxy/tags/0.0.1) (2020-12-03)

**Added:** initial files
