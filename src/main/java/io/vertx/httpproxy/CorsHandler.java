package io.vertx.httpproxy;


import io.vertx.codegen.annotations.Fluent;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.httpproxy.impl.CorsHandlerImpl;


import java.util.Set;

/**
 * A handler which implements server side http://www.w3.org/TR/cors/[CORS] support for Vert.x-Web.
 * Based on Work by
 * @author <a href="http://tfox.org">Tim Fox</a>
 */
public interface CorsHandler {

  /**
   * Create a CORS handler
   *
   * @param allowedOriginPattern  the allowed origin pattern
   * @return  the handler
   */
  static CorsHandler create(String allowedOriginPattern) {
    return new CorsHandlerImpl(allowedOriginPattern);
  }



  /**
   * Add a set of  allowed methods
   *
   * @param methods the methods to add
   * @return a reference to this, so the API can be used fluently
   */
  @Fluent
  CorsHandler allowedMethods(Set<HttpMethod> methods);



  /**
   * Add a set of allowed headers
   *
   * @param headerNames  the allowed header names
   * @return a reference to this, so the API can be used fluently
   */
  @Fluent
  CorsHandler allowedHeaders(Set<String> headerNames);



  /**
   * Add a set of exposed headers
   *
   * @param headerNames  the exposed header names
   * @return a reference to this, so the API can be used fluently
   */
  @Fluent
  CorsHandler exposedHeaders(Set<String> headerNames);

  /**
   * Set whether credentials are allowed. Note that user agents will block
   * requests that use a wildcard as origin and include credentials.
   *
   * From the MDN documentation you can read:
   *
   * <blockquote>
   * Important note: when responding to a credentialed request,
   * server must specify a domain, and cannot use wild carding.
   * </blockquote>
   *
   * @param allow true if allowed
   * @return a reference to this, so the API can be used fluently
   */
  @Fluent
  CorsHandler allowCredentials(boolean allow);


  public void corsHeaders(HttpServerResponse outboundResponse, HttpServerRequest outboundRequest);
  public boolean isValidOrigin(HttpServerRequest outboundRequest);
}
