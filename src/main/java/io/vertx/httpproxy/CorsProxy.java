/*
 * Copyright (c) 2011-2020 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 * which is available at https://www.apache.org/licenses/LICENSE-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 */
package io.vertx.httpproxy;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.httpproxy.impl.CorsProxyImpl;

/**
 * Handles the HTTP reverse proxy logic between the <i><b>user agent</b></i> and the <i><b>origin</b></i>.
 * <p>
 * @author <a href="mailto:julien@julienviet.com">Julien Viet</a>
 */

public interface CorsProxy extends Handler<HttpServerRequest> {

  /**
   * Create a new {@code HttpProxy} instance.
   *
   * @param client the {@code HttpClient} that forwards <i><b>outbound</b></i> requests to the <i><b>origin</b></i>.
   * @param corsHandler
   * @return a reference to this, so the API can be used fluently.
   */
  static CorsProxy reverseProxy2(HttpClient client, CorsHandler corsHandler) {
    return new CorsProxyImpl(client,corsHandler);
  }



  /**
   * Handle the <i><b>outbound</b></i> {@code HttpServerRequest}.
   *
   * @param outboundRequest the outbound {@code HttpServerRequest}
   */
  void handle(HttpServerRequest outboundRequest);

}
