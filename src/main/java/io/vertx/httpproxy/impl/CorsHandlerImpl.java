package io.vertx.httpproxy.impl;


import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.httpproxy.CorsHandler;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import static io.vertx.core.http.HttpHeaders.*;
import static io.vertx.core.http.HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS;

/**
 * Based on io.vertx.ext.web.handler.impl.CorsHandlerImpl.java
 * Based partially on original authored by David Dossot
 *
 * @author <a href="david@dossot.net">David Dossot</a>
 * @author <a href="http://tfox.org">Tim Fox</a>
 */

public class CorsHandlerImpl implements CorsHandler {

    private final Pattern allowedOrigin;

    private String allowedMethodsString;
    private String allowedHeadersString;
    private String exposedHeadersString;
    private boolean allowCredentials;
    private final Set<HttpMethod> allowedMethods = new LinkedHashSet<>();
    private final Set<String> allowedHeaders = new LinkedHashSet<>();
    private final Set<String> exposedHeaders = new LinkedHashSet<>();


    public CorsHandlerImpl(String allowedOriginPattern) {
        Objects.requireNonNull(allowedOriginPattern);
        if ("*".equals(allowedOriginPattern)) {
            allowedOrigin = null;
        } else {
            allowedOrigin = Pattern.compile(allowedOriginPattern);
        }
    }

    public CorsHandler allowedMethods(Set<HttpMethod> methods) {
        allowedMethods.addAll(methods);
        allowedMethodsString = join(allowedMethods);
        return this;
    }

    public CorsHandler allowedHeaders(Set<String> headerNames) {
        allowedHeaders.addAll(headerNames);
        allowedHeadersString = join(allowedHeaders);
        return this;
    }

    public CorsHandler exposedHeaders(Set<String> headerNames) {
        exposedHeaders.addAll(headerNames);
        exposedHeadersString = join(exposedHeaders);
        return this;
    }

    public CorsHandler allowCredentials(boolean allow) {
        if (allowedOrigin == null && allow) {
            // https://developer.mozilla.org/En/HTTP_access_control#Requests_with_credentials
            throw new IllegalStateException("wildcard origin with credentials is not allowed");
        }
        this.allowCredentials = allow;
        return this;
    }

    public void corsHeaders(HttpServerResponse outboundResponse, HttpServerRequest outboundRequest) {


        String origin = outboundRequest.headers().get(ORIGIN);

        if (origin == null) {
            // Not a CORS request - we don't set any headers and just call the next handler

        } else if (isValidOrigin(origin)) {
            String accessControlRequestMethod = outboundRequest.headers().get(ACCESS_CONTROL_REQUEST_METHOD);
            if (outboundRequest.method() == HttpMethod.OPTIONS && accessControlRequestMethod != null) {
                // Pre-flight request
                addCredentialsAndOriginHeader(outboundResponse, origin);
                if (allowedMethodsString != null) {
                    outboundResponse.putHeader(ACCESS_CONTROL_ALLOW_METHODS, allowedMethodsString);
                }
                if (allowedHeadersString != null) {
                    outboundResponse.putHeader(ACCESS_CONTROL_ALLOW_HEADERS, allowedHeadersString);
                }

                // according to MDC although the is no body the response should be OK
                outboundResponse.setStatusCode(200).end();
            } else {
                addCredentialsAndOriginHeader(outboundResponse, origin);
                if (exposedHeadersString != null) {
                    outboundResponse.putHeader(ACCESS_CONTROL_EXPOSE_HEADERS, exposedHeadersString);
                }

            }
        } else {
            outboundResponse
                    .setStatusMessage("CORS Rejected - Invalid origin");

        }

    }


    private void addCredentialsAndOriginHeader(HttpServerResponse response, String origin) {
        if (allowCredentials) {
            response.putHeader(ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
        }

        //we only allow exact origin (not '*')
        response.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN, "*");

    }


    public boolean isValidOrigin(HttpServerRequest outboundRequest){
        String origin = outboundRequest.headers().get(ORIGIN);
        if(origin==null){
            return false;
        }
        return  isValidOrigin(origin);
    }

    private boolean isValidOrigin(String origin) {
        if (allowedOrigin == null) {
            // Null means accept all origins
            return true;
        }


        return allowedOrigin.matcher(origin).matches();
    }

    private String join(Collection<?> ss) {
        if (ss == null || ss.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (Object s : ss) {
            if (!first) {
                sb.append(',');
            }
            sb.append(s);
            first = false;
        }
        return sb.toString();
    }
}
