package io.piveau.utils.corsproxy

import io.piveau.json.ConfigHelper
import io.vertx.config.ConfigRetriever
import io.vertx.core.Launcher
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.httpproxy.CorsHandler
import io.vertx.httpproxy.CorsProxy
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import java.util.*
import kotlin.collections.HashSet

//TODO: only get?
//TODO: only with origin?

//TODO: security? ->
// Allowed mime types?
// Disable coockies credentials allowed=false?

//TODO als post vllt?


class MainVerticle : CoroutineVerticle() {


    override suspend fun start() {

        val proxyClient = vertx.createHttpClient()


        val config = ConfigRetriever.create(vertx).config.coAwait()


        val corsHandler = getCorsHandler(config)


        val backend = CorsProxy.reverseProxy2(proxyClient, corsHandler)


        val proxyServer = vertx.createHttpServer()


        val port = config.getInteger("PROXY_PORT", 8080)
        proxyServer.requestHandler(backend).listen(port)


    }


}


fun getCorsHandler(conf: JsonObject): CorsHandler {

    val corsDomains: JsonArray = ConfigHelper.forConfig(conf).forceJsonArray("PROXY_CORS_DOMAINS")

    println(corsDomains.encodePrettily())


    if (!corsDomains.isEmpty) {
        val allowedHeaders: MutableSet<String> = HashSet()
        allowedHeaders.add("x-requested-with")
        allowedHeaders.add("Access-Control-Allow-Origin")
        allowedHeaders.add("origin")
        allowedHeaders.add("Content-Type")
        allowedHeaders.add("accept")
        allowedHeaders.add("Authorization")
        allowedHeaders.add("Access-Control-Allow-Headers")
        allowedHeaders.add("X-Auth-Token")


        val allowedMethods: MutableSet<HttpMethod> = HashSet()
        allowedMethods.add(HttpMethod.GET)
        allowedMethods.add(HttpMethod.POST)
        allowedMethods.add(HttpMethod.OPTIONS)
        allowedMethods.add(HttpMethod.DELETE)
        allowedMethods.add(HttpMethod.PATCH)
        allowedMethods.add(HttpMethod.PUT)

        val exposedHeaders: MutableSet<String> = HashSet()
        exposedHeaders.add("Content-Disposition")


        val corsArray = ArrayList<String>()
        for (i in 0 until corsDomains.size()) {
            //convert into normal array and escape dots for regex compatibility
            corsArray.add(corsDomains.getString(i).replace(".", "\\."))
        }

        //"^(https?:\\/\\/(?:.+\\.)?(?:fokus\\.fraunhofer\\.de|localhost)(?::\\d{1,5})?)$"
        val corsString = "^(https?:\\/\\/(?:.+\\.)?(?:" + java.lang.String.join("|", corsArray) + ")(?::\\d{1,5})?)$"
        return CorsHandler.create(corsString).allowedHeaders(allowedHeaders)
            .allowedMethods(allowedMethods).exposedHeaders(exposedHeaders)

    } else {
        throw IllegalStateException("no CORS domains configured")

    }

}


fun main(args: Array<String>) {
    Launcher.executeCommand("run", *(args.plus(MainVerticle::class.java.name)))
}
